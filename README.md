# Microservices transaction

This is an assessment project for apply as backend engineer for backbase.com.
You can find a trace of the assessment in the application folder.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
 
### Prerequisites

- **Required**: Java JRE 8
- **Optional**: Docker

The application is built with Java **8**, you can use [SDKMAN](https://sdkman.io/) to install the necessary runtime environment. The JDK version that has been used is:  **8.0.212-zulu**
Install Docker if you would like to run it into a container.

### Installing

- Checkout the git repo:  
``git clone https://fabdamico@bitbucket.org/fabdamico/microservice
transaction.git``

- cd into the cloned folder: ``` cd microservice-transaction```

- build the application using the gradle wrapper: ```./gradlew build```
this task will generate into ``microservice-transaction/build/libs`` folder
a runnable jar file: ``microservice-transaction-1.0.0.jar``

- run the jar: ``java -jar microservice-transaction-1.0.0.jar``
the application is exposed on ``localhost:8080``

### Docker
The application is built to run into a docker container.

1. Run this gradle task within the root project folder to build a docker image:
``./gradlew build docker``

2. execute this gradle task to run a container based on the docker image created at step 1:
``./gradlew dockerRun``

This has been achieved using a great Gradle docker plugin created by [Palantir](https://github.com/palantir/gradle-docker).
Are available more task to *stop*, *remove* and *status* the container created at step 1.

## Using the application

At this *public* url: [localhost:8080/doc](localhost:8080/doc) you will find a documentation for the exposed endpoint.

### Security
The application is secured with *basic access authentication*.
An anonymous user can access [localhost:8080/doc](localhost:8080/doc) page. 
The browser will enforce for authentication but is not necessary.
For any other endpoint the credentials are:

>   **user**: backbase-user     
>   **password**: 5Qb&kNq)Lb5S}%[W

## Development note
If you import this project into an IDE like *Intellij/Eclipse* be sure to have installed the **lombok plugin** more info here: [Lombok](https://projectlombok.org/setup/intellij) and remember to have enabled the `annotation processor` otherwise the IDE is not able to compile properly. 
**N.B** This step is not required in order to build and run the app only if imported into an IDE.

## Test
The project contains some *unit* and *integration* tests. By default the unit tests mock their dependencies because they should be able to run always. 
There are also some integration tests depending on Open Bank api but they are excluded in the CI. 
Every push on git will trigger a build pipeline into bitbucket (where is hosted this project) that will run the unit test and ensure that the logic is not broken.

## Test data
Due to the scope of this assessment the open bank api sandbox provided is for one bank, one account and few type of transaction. 
Use the following test data:
- As **accountId**: savings-kids-john
- As **bank**: rbs
- As **type**: SANDBOX_TAN or sandbox-payment