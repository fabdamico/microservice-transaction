package com.backbase.assessment.mapper;


import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.Transaction;
import com.backbase.assessment.model.TransactionLegacy;
import com.backbase.assessment.model.TransactionResource;
import com.backbase.assessment.model.TransactionsResource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class TransactionMapper {

  public TransactionsResource transactionResourceToTransactionLegacy(
      TransactionLegacy transactionLegacy) throws MicroServiceTransactionException {
    try {
      List<TransactionResource> transactionResources = transactionLegacy.getTransactions()
          .stream()
          .map(this::transactionToTransactionResource)
          .collect(Collectors.toList());

      return TransactionsResource.builder().transactions(transactionResources).build();
    } catch (Exception e) {
      throw new MicroServiceTransactionException(e);
    }
  }

  private TransactionResource transactionToTransactionResource(Transaction transaction) {
    return TransactionResource.builder()
        .transactionType(transaction.getDetails().getType())
        .transactionAmount(Double.valueOf(transaction.getDetails().getValue().getAmount()))
        .transactionCurrency(transaction.getDetails().getValue().getCurrency())
        .instructedCurrency(transaction.getDetails().getValue().getCurrency())
        .instructedAmount(Double.valueOf(transaction.getDetails().getValue().getAmount()))
        .id(transaction.getId())
        .description(transaction.getDetails().getDescription())
        .counterPartyName(transaction.getOtherAccount().getHolder().getName())
        .counterPartyLogoPath(Optional
            .ofNullable(transaction.getOtherAccount().getMetadata().getImageURL())
            .map(Object::toString).orElse(null)
        )
        .counterPartyAccount(transaction.getOtherAccount().getNumber())
        .accountId(transaction.getOtherAccount().getId())
        .build();
  }
}
