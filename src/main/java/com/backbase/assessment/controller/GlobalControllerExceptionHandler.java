package com.backbase.assessment.controller;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.ErrorResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

  @ExceptionHandler(MicroServiceTransactionException.class)
  public ResponseEntity<ErrorResponse> handleNomadBusinessException(
      MicroServiceTransactionException exception) {
    ErrorResponse errorResponse = ErrorResponse.builder()
        .message(exception.getMessage())
        .statusCode(exception.getHttpStatus().value())
        .timestamp(LocalDateTime.now()
            .format((DateTimeFormatter.ISO_LOCAL_DATE_TIME)))
        .build();
    return new ResponseEntity<>(errorResponse, exception.getHttpStatus());
  }
}
