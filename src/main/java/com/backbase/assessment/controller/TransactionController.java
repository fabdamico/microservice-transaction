package com.backbase.assessment.controller;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.TotalAmount;
import com.backbase.assessment.model.TransactionsResource;
import com.backbase.assessment.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/v1/")
public class TransactionController {

  private TransactionService transactionService;

  @Autowired
  public TransactionController(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @GetMapping("{bank}/{accountId}/transactions")
  public TransactionsResource getTransactionsByType(@PathVariable String accountId,
      @PathVariable String bank,
      @RequestParam(value = "type", required = false) String type)
      throws MicroServiceTransactionException {
    if (type == null) {
      return transactionService.getTransactionsById(bank, accountId);
    }
    return transactionService.getTransactionsByIdAndType(bank, accountId, type);
  }

  @GetMapping("{bank}/{accountId}/transactions/amount")
  public TotalAmount getTotalAmountForTransactionType(@PathVariable String accountId,
      @PathVariable String bank,
      @RequestParam(value = "type") String type)
      throws MicroServiceTransactionException {
    return transactionService.getTransactionTotalAmountByType(bank, accountId, type);
  }
}
