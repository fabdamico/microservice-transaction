package com.backbase.assessment.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@Slf4j
public class MicroServiceTransactionException extends Exception {

  private String message;
  private HttpStatus httpStatus;

  public MicroServiceTransactionException(Exception e, HttpStatus httpsStatusCode) {
    super(e);
    this.setMessage(e.getMessage());
    this.httpStatus = httpsStatusCode;
    log.error(e.getMessage());
  }

  public MicroServiceTransactionException(Exception e) {
    super(e);
    this.setMessage(e.getMessage());
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    log.error(e.getMessage());
  }
}
