package com.backbase.assessment.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ErrorResponse {

  private String message;
  private int statusCode;
  private String timestamp;
}
