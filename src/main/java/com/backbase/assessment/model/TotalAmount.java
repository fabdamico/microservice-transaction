package com.backbase.assessment.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TotalAmount {

  private String type;
  private Double amount;
  private String account;
  private String bank;
}
