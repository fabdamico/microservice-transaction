package com.backbase.assessment.model;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TransactionsResource {

  private List<TransactionResource> transactions;
}
