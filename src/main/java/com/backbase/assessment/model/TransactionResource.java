package com.backbase.assessment.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TransactionResource {

  private String id;
  private String accountId;
  private String counterPartyAccount;
  private String counterPartyName;
  private String counterPartyLogoPath;
  private Double instructedAmount;
  private String instructedCurrency;
  private Double transactionAmount;
  private String transactionCurrency;
  private String transactionType;
  private String description;
}
