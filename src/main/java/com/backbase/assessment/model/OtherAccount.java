package com.backbase.assessment.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "holder",
    "number",
    "kind",
    "IBAN",
    "swift_bic",
    "bank",
    "metadata"
})
public class OtherAccount {

  @JsonProperty("id")
  private String id;
  @JsonProperty("holder")
  private HolderOtherAccount holder;
  @JsonProperty("number")
  private String number;
  @JsonProperty("kind")
  private Object kind;
  @JsonProperty("IBAN")
  private Object iBAN;
  @JsonProperty("swift_bic")
  private Object swiftBic;
  @JsonProperty("bank")
  private BankOtherAccount bank;
  @JsonProperty("metadata")
  private Metadata metadata;

  @JsonProperty("id")
  public String getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(String id) {
    this.id = id;
  }

  @JsonProperty("holder")
  public HolderOtherAccount getHolder() {
    return holder;
  }

  @JsonProperty("holder")
  public void setHolder(HolderOtherAccount holder) {
    this.holder = holder;
  }

  @JsonProperty("number")
  public String getNumber() {
    return number;
  }

  @JsonProperty("number")
  public void setNumber(String number) {
    this.number = number;
  }

  @JsonProperty("kind")
  public Object getKind() {
    return kind;
  }

  @JsonProperty("kind")
  public void setKind(Object kind) {
    this.kind = kind;
  }

  @JsonProperty("IBAN")
  public Object getIBAN() {
    return iBAN;
  }

  @JsonProperty("IBAN")
  public void setIBAN(Object iBAN) {
    this.iBAN = iBAN;
  }

  @JsonProperty("swift_bic")
  public Object getSwiftBic() {
    return swiftBic;
  }

  @JsonProperty("swift_bic")
  public void setSwiftBic(Object swiftBic) {
    this.swiftBic = swiftBic;
  }

  @JsonProperty("bank")
  public BankOtherAccount getBank() {
    return bank;
  }

  @JsonProperty("bank")
  public void setBank(BankOtherAccount bank) {
    this.bank = bank;
  }

  @JsonProperty("metadata")
  public Metadata getMetadata() {
    return metadata;
  }

  @JsonProperty("metadata")
  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }

}
