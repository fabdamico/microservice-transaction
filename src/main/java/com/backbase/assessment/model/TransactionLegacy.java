package com.backbase.assessment.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "transactions"
})
public class TransactionLegacy {

  @JsonProperty("transactions")
  private List<Transaction> transactions = null;

  @JsonProperty("transactions")
  public List<Transaction> getTransactions() {
    return transactions;
  }

  @JsonProperty("transactions")
  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }

}
