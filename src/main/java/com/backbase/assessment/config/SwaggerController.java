package com.backbase.assessment.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SwaggerController {

  @GetMapping("/doc")
  public String greeting() {
    return "redirect:/swagger-ui.html";
  }
}
