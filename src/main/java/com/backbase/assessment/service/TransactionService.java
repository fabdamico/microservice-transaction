package com.backbase.assessment.service;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.TotalAmount;
import com.backbase.assessment.model.TransactionsResource;

public interface TransactionService {

  TransactionsResource getTransactionsById(String bank, String accountId)
      throws MicroServiceTransactionException;

  TransactionsResource getTransactionsByIdAndType(String bank, String accountId, String type)
      throws MicroServiceTransactionException;

  TotalAmount getTransactionTotalAmountByType(String bank, String accountId, String type)
      throws MicroServiceTransactionException;
}
