package com.backbase.assessment.service;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.TransactionLegacy;

public interface HttpClientService {

  TransactionLegacy getTransactionsByIdAndBank(String bank, String accountId)
      throws MicroServiceTransactionException;
}
