package com.backbase.assessment.service.impl;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.model.TransactionLegacy;
import com.backbase.assessment.service.HttpClientService;
import java.net.URI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service
public class OBPHttpClientServiceImpl implements HttpClientService {

  private static final String HTTPS = "https";
  private RestTemplate restTemplate;

  @Value("${obp.base-url}")
  private String baseUrl;
  @Value("${obp.transaction-endpoint}")
  private String transactionEndpoint;

  @Autowired
  public OBPHttpClientServiceImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public TransactionLegacy getTransactionsByIdAndBank(String bank, String accountId)
      throws MicroServiceTransactionException {
    try {
      return restTemplate
          .getForObject(buildUriFromTemplate(bank, accountId), TransactionLegacy.class);
    } catch (HttpStatusCodeException e) {
      throw new MicroServiceTransactionException(e, e.getStatusCode());
    }
  }

  private URI buildUriFromTemplate(String bank, String account) {
    UriComponents uriComponents = UriComponentsBuilder.newInstance()
        .scheme(HTTPS)
        .host(baseUrl)
        .path(transactionEndpoint)
        .buildAndExpand(bank, account);
    log.info("URI builder: {}", uriComponents.toString());
    return uriComponents.toUri();
  }
}
