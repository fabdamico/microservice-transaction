package com.backbase.assessment.service.impl;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.mapper.TransactionMapper;
import com.backbase.assessment.model.TotalAmount;
import com.backbase.assessment.model.Transaction;
import com.backbase.assessment.model.TransactionLegacy;
import com.backbase.assessment.model.TransactionsResource;
import com.backbase.assessment.service.HttpClientService;
import com.backbase.assessment.service.TransactionService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TransactionServiceImpl implements TransactionService {

  private TransactionMapper transactionMapper;
  private HttpClientService httpClientService;

  @Autowired
  public TransactionServiceImpl(TransactionMapper transactionMapper,
      HttpClientService httpClientService) {
    this.transactionMapper = transactionMapper;
    this.httpClientService = httpClientService;
  }

  @Override
  public TransactionsResource getTransactionsById(String bank, String accountId)
      throws MicroServiceTransactionException {
    TransactionLegacy transactionLegacy = httpClientService
        .getTransactionsByIdAndBank(bank, accountId);
    return transactionMapper.transactionResourceToTransactionLegacy(transactionLegacy);
  }

  @Override
  public TransactionsResource getTransactionsByIdAndType(String bank, String accountId, String type)
      throws MicroServiceTransactionException {
    TransactionLegacy transactionLegacy = httpClientService
        .getTransactionsByIdAndBank(bank, accountId);

    List<Transaction> transactionListFilteredByType = filterTransactionByType(type,
        transactionLegacy)
        .collect(Collectors.toList());

    transactionLegacy.setTransactions(transactionListFilteredByType);
    return transactionMapper.transactionResourceToTransactionLegacy(transactionLegacy);
  }

  @Override
  public TotalAmount getTransactionTotalAmountByType(String bank, String accountId, String type)
      throws MicroServiceTransactionException {
    TransactionLegacy transactionLegacy = httpClientService
        .getTransactionsByIdAndBank(bank, accountId);

    Double totalAmount = filterTransactionByType(type, transactionLegacy)
        .mapToDouble(transaction -> Double
            .parseDouble(transaction.getDetails().getValue().getAmount()))
        .sum();
    return TotalAmount
        .builder()
        .account(accountId)
        .bank(bank)
        .amount(totalAmount)
        .type(type)
        .build();
  }

  private Stream<Transaction> filterTransactionByType(String type,
      TransactionLegacy transactionLegacy) {
    return transactionLegacy.getTransactions()
        .stream()
        .filter(transaction -> transaction.getDetails().getType() != null)
        .filter(transaction -> transaction.getDetails().getType().equals(type));
  }
}
