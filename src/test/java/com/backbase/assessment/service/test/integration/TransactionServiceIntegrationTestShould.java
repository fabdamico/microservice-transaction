package com.backbase.assessment.service.test.integration;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.mapper.TransactionMapper;
import com.backbase.assessment.model.TotalAmount;
import com.backbase.assessment.model.TransactionsResource;
import com.backbase.assessment.service.impl.OBPHttpClientServiceImpl;
import com.backbase.assessment.service.impl.TransactionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;


public class TransactionServiceIntegrationTestShould {

  private static final String SAVINGS_KIDS_JOHN = "savings-kids-john";
  private static final String SANDBOX_TAN = "SANDBOX_TAN";
  private static final String SANDBOX_PAYMENTS = "sandbox-payment";

  private TransactionServiceImpl obpClientService;
  private RestTemplate restTemplate;
  private TransactionMapper transactionMapper;
  private OBPHttpClientServiceImpl obpHttpClientService;

  @Before
  public void setUp() {
    restTemplate = new RestTemplate();
    transactionMapper = new TransactionMapper();
    obpHttpClientService = new OBPHttpClientServiceImpl(restTemplate);
    obpClientService = new TransactionServiceImpl(transactionMapper, obpHttpClientService);
    ReflectionTestUtils.setField(obpHttpClientService, "baseUrl",
        "apisandbox.openbankproject.com/obp/v1.2.1/banks/{bank}");
    ReflectionTestUtils.setField(obpHttpClientService, "transactionEndpoint",
        "/accounts/{account-id}/public/transactions");
  }

  @Test
  public void return_transactions_by_given_id_and_bank()
      throws MicroServiceTransactionException {
    TransactionsResource transactions =
        obpClientService.getTransactionsById("rbs", SAVINGS_KIDS_JOHN);
    assertThat(transactions.getTransactions().size(), is(50));
    assertThat(transactions.getTransactions().get(0).getCounterPartyAccount(),
        is(SAVINGS_KIDS_JOHN));
  }

  @Test
  public void return_total_amount_for_transaction_filtered_by_type()
      throws MicroServiceTransactionException {
    TotalAmount totalAmountByType =
        obpClientService.getTransactionTotalAmountByType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_TAN);
    assertThat(totalAmountByType.getBank(), is("rbs"));
    assertThat(totalAmountByType.getAccount(), is(SAVINGS_KIDS_JOHN));
    assertThat(totalAmountByType.getType(), is(SANDBOX_TAN));
    assertThat(totalAmountByType.getAmount(), is(10.0));

    totalAmountByType = obpClientService
        .getTransactionTotalAmountByType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_PAYMENTS);
    assertThat(totalAmountByType.getBank(), is("rbs"));
    assertThat(totalAmountByType.getAccount(), is(SAVINGS_KIDS_JOHN));
    assertThat(totalAmountByType.getType(), is(SANDBOX_PAYMENTS));
    assertThat(totalAmountByType.getAmount(), is(73.76));
  }

  @Test
  public void return_transactions_by_given_id_bank_filtered_by_type()
      throws MicroServiceTransactionException {
    TransactionsResource transactions =
        obpClientService.getTransactionsByIdAndType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_TAN);
    assertThat(transactions.getTransactions().size(), is(2));
    assertThat(transactions.getTransactions().get(0).getCounterPartyAccount(),
        is(SAVINGS_KIDS_JOHN));
    assertThat(transactions.getTransactions().get(0).getTransactionType(),
        is(SANDBOX_TAN));
    assertThat(transactions.getTransactions().get(1).getTransactionType(),
        is(SANDBOX_TAN));
  }

  @Test(expected = MicroServiceTransactionException.class)
  public void return_microServiceTransactionException_given_a_wrong_bank()
      throws MicroServiceTransactionException {
    obpClientService.getTransactionsById("rbss", SAVINGS_KIDS_JOHN);
  }
}