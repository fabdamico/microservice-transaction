package com.backbase.assessment.service.test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.backbase.assessment.exception.MicroServiceTransactionException;
import com.backbase.assessment.mapper.TransactionMapper;
import com.backbase.assessment.model.TotalAmount;
import com.backbase.assessment.model.TransactionLegacy;
import com.backbase.assessment.model.TransactionsResource;
import com.backbase.assessment.service.impl.OBPHttpClientServiceImpl;
import com.backbase.assessment.service.impl.TransactionServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.function.Function;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTestShould {

  private static final String SAVINGS_KIDS_JOHN = "savings-kids-john";
  private static final String SANDBOX_TAN = "SANDBOX_TAN";
  private static final String SANDBOX_PAYMENTS = "sandbox-payment";

  @InjectMocks
  private TransactionServiceImpl transactionService;
  @Mock
  private OBPHttpClientServiceImpl obpHttpClientService;

  @Before
  public void setUp() {
    TransactionMapper transactionMapper = new TransactionMapper();
    transactionService = new TransactionServiceImpl(transactionMapper, obpHttpClientService);
  }

  @Test
  public void return_transactions_by_given_id_and_bank()
      throws MicroServiceTransactionException, IOException {

    when(obpHttpClientService.getTransactionsByIdAndBank(anyString(), anyString()))
        .thenReturn(generateTransactionsFromJsonFile());

    TransactionsResource transactions =
        transactionService.getTransactionsById("rbs", SAVINGS_KIDS_JOHN);
    assertThat(transactions.getTransactions().size(), is(50));
    assertThat(transactions.getTransactions().get(0).getCounterPartyAccount(),
        is(SAVINGS_KIDS_JOHN));

    verify(obpHttpClientService, atLeastOnce())
        .getTransactionsByIdAndBank(anyString(), anyString());
  }

  @Test
  public void return_total_amount_for_transaction_filtered_by_type()
      throws MicroServiceTransactionException, IOException {

    when(obpHttpClientService.getTransactionsByIdAndBank(anyString(), anyString()))
        .thenReturn(generateTransactionsFromJsonFile());

    TotalAmount totalAmountByType =
        transactionService.getTransactionTotalAmountByType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_TAN);
    assertThat(totalAmountByType.getBank(), is("rbs"));
    assertThat(totalAmountByType.getAccount(), is(SAVINGS_KIDS_JOHN));
    assertThat(totalAmountByType.getType(), is(SANDBOX_TAN));
    assertThat(totalAmountByType.getAmount(), is(10.0));

    totalAmountByType = transactionService
        .getTransactionTotalAmountByType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_PAYMENTS);
    assertThat(totalAmountByType.getBank(), is("rbs"));
    assertThat(totalAmountByType.getAccount(), is(SAVINGS_KIDS_JOHN));
    assertThat(totalAmountByType.getType(), is(SANDBOX_PAYMENTS));
    assertThat(totalAmountByType.getAmount(), is(73.76));

    verify(obpHttpClientService, atLeastOnce())
        .getTransactionsByIdAndBank(anyString(), anyString());
  }

  @Test
  public void return_transactions_by_given_id_bank_filtered_by_type()
      throws MicroServiceTransactionException, IOException {

    when(obpHttpClientService.getTransactionsByIdAndBank(anyString(), anyString()))
        .thenReturn(generateTransactionsFromJsonFile());

    TransactionsResource transactions =
        transactionService.getTransactionsByIdAndType("rbs", SAVINGS_KIDS_JOHN, SANDBOX_TAN);
    assertThat(transactions.getTransactions().size(), is(2));
    assertThat(transactions.getTransactions().get(0).getCounterPartyAccount(),
        is(SAVINGS_KIDS_JOHN));
    assertThat(transactions.getTransactions().get(0).getTransactionType(),
        is(SANDBOX_TAN));
    assertThat(transactions.getTransactions().get(1).getTransactionType(),
        is(SANDBOX_TAN));

    verify(obpHttpClientService, atLeastOnce())
        .getTransactionsByIdAndBank(anyString(), anyString());
  }

  @Test(expected = MicroServiceTransactionException.class)
  public void return_microServiceTransactionException_given_a_wrong_bank()
      throws MicroServiceTransactionException {
    when(obpHttpClientService.getTransactionsByIdAndBank(anyString(), anyString()))
        .thenThrow(MicroServiceTransactionException.class);

    transactionService.getTransactionsById("rbss", SAVINGS_KIDS_JOHN);

    verify(obpHttpClientService, atLeastOnce())
        .getTransactionsByIdAndBank(anyString(), anyString());
  }

  private TransactionLegacy generateTransactionsFromJsonFile() throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    File transactions = getCustomerFileReader.apply("transactions.json");
    return objectMapper.readValue(transactions, TransactionLegacy.class);
  }

  private Function<String, File> getCustomerFileReader = filename -> {
    ClassLoader cl = getClass().getClassLoader();
    return new File(cl.getResource(filename).getFile());
  };
}
